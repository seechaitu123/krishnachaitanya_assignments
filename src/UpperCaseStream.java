
import java.util.*;
import java.util.stream.Collectors;

public class UpperCaseStream {
    public static void main(String args[]){
        List<String> strings = Arrays.asList("USA","japan","france","Germany","U.k","India");
        System.out.println(strings.stream().map(i->i.toUpperCase()).collect(Collectors.toList()));
    }
}
