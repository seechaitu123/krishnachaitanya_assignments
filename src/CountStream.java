
import java.util.*;
import java.util.stream.Collectors;

public class CountStream {
    public static void main(String args[]){
        List<Integer> integers = Arrays.asList(1,2,2,4,2,5);
        System.out.println(integers.stream().filter(integer -> integer==2).count());
    }
}
