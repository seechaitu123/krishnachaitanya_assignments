import java.util.*;
import java.util.stream.Collectors;

public class SquareStream {
    public static void main(String args[]){
        List<Integer> integers = Arrays.asList(9,10,3,4,7,3,4);
        System.out.println(integers.stream().distinct().map(i->i*i).collect(Collectors.toList()));
    }
}
