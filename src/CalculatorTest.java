import java.util.*;

public class CalculatorTest {
    public static void main(String args[]){
        System.out.println("Enter Two Numbers");
        double number1 = new Scanner(System.in).nextDouble();
        double number2 = new Scanner(System.in).nextDouble();

        Calculator calculator = new Calculator();
        //Scanner input = new Scanner(System.in);
        System.out.println("Enter the operation");
        String operation = new Scanner(System.in).nextLine();
        switch(operation){
            case "add":
                System.out.println(calculator.addition(number1,number2));
                break;
            case "sub":
                System.out.println(calculator.subtraction(number1,number2));
                break;
            case "mul":
                System.out.println(calculator.multipication(number1,number2));
                break;
            case "div":
                System.out.println(calculator.division(number1,number2));
                break;
            default:
                System.out.println("no operation:");
                break;
        }
    }
}

