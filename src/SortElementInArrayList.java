/*import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class SortElementInArrayList {

    public static void main(String[] args) {
        Map<String, ArrayList> mapItems = new HashMap<>();
        ArrayList<String> items = new ArrayList<>();
        items.add("skirts");
        items.add("leggings");
        items.add("shorts");

        mapItems.put("Reebok", items);
        mapItems.put("Adidas", items);
        mapItems.put("Nike", items);
        mapItems.put("American tourister", items);
        mapItems.put("Boxer", items);

        System.out.println("Demo of sort methods: ");
        demoSortMethod(mapItems);
    }

    private static void demoSortMethod(Map<String, ArrayList> mapItems) {
        // {Tennis=Federer, Cricket=Bradman, Golf=Woods, Basketball=Jordan, Boxer=Ali}
        System.out.println("Orignal HashMap:" + mapItems);

        System.out.println("\n1. Sort HashMap by ascending keys: " );
       HashMap<String,String>mapSorted = new HashMap<>(mapItems);
        mapSorted.forEach((key, value) -> {
            System.out.println(key + ", " + value);
        });

        System.out.println("\n2. Sort HashMap by descending keys: " );
        mapSorted.descendingMap().forEach((key, value) -> {
            System.out.println( key + ", "  + value);
        });
    }
}*/