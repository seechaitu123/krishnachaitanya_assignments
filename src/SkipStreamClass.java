import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SkipStreamClass {
        public static void main(String args[]){
            List<Integer> integers = Arrays.asList(2,4,5,9,10,11,12,13,14,17,20);
            System.out.println(integers.stream().skip(9).collect(Collectors.toList()));
        }
}
