package collections;
import java.util.*;

public class TreeSetClass {
    public static void main(String args[]){

        TreeSet<String> treeset = new TreeSet<String>();
        treeset.add("Chaitu");
        treeset.add("Praveen");
        treeset.add("Lavdu");
        treeset.add("Praveen");
        treeset.add("Sow");

        Iterator itr = treeset.iterator();
        while(itr.hasNext()){
            System.out.println(itr.next());
        }
    }
}
