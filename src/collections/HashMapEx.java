package collections;
import java.util.*;

public class HashMapEx {
    public static void main(String args[]){
    HashMap<Integer,String> hm = new HashMap<Integer,String>();
    hm.put(1,"chaitu");
    hm.put(2,"prav");
    hm.put(3,"nav");

    for(Map.Entry e:hm.entrySet()){
        System.out.println(e.getKey()+"  "+e.getValue());
    }
  }
}