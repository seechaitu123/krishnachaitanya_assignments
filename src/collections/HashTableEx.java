package collections;

import java.util.*;

public class HashTableEx {
    public static void main(String args[]){
        Hashtable<Integer,String> ht = new Hashtable<Integer,String>();
        ht.put(1,"chaitu");
        ht.put(2,"prav");
        ht.put(3,"nav");

        for(Map.Entry e:ht.entrySet()){
            System.out.println(e.getKey()+"  "+e.getValue());
        }
    }
}
