package collections;

import java.util.Iterator;
import java.util.LinkedHashSet;


public class LinkedHashSetClass {
    public static void main(String args[]) {

        LinkedHashSet<String> lh = new LinkedHashSet<String>();
        lh.add("Chaitu");
        lh.add("Praveen");
        lh.add("Lavdu");
        lh.add("Praveen");
        lh.add("Sow");

         Iterator itr = lh.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }
}
