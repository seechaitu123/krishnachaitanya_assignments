package collections;

public class Employee {

    int employeeId;
    String empname;
    int salary;

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public void setEmpname(String empname) {
        this.empname = empname;
    }

    public String getEmpname() {
        return empname;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void Student(int employeeId, String empname, int salary){

        this.employeeId=employeeId;
        this.empname=empname;
        this.salary=salary;
    }

}
